<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<body>
    <form action="{{ route('request') }}" method="POST">
        @csrf
        <label for="etudiant">Etudiant :</label>
        <select name="etudiant" id = "etudiant">
            @foreach ($etudiants as $etudiant)
                <option value="{{ $etudiant->id }}">{{ $etudiant->nom }} {{ $etudiant->prenom }}</option>
            @endforeach
        </select>
        <label for="convention">Convention :</label>
        <!--TODO: do this in JS in front end to avoid reloading the page-->
        @foreach($conventions as $convention)
            @if($etudiant->convention == $convention->id)
                <input type="text" name="convention" placeholder="{{ $convention->nom }}" disabled>
            @endif
        @endforeach
        <br><br>
        <label for="message">Attestation :</label>
        <textarea name="message" id="message"></textarea>
        <input type="submit" value="Valider">
    </form>
</body>
